package u04lab.code

import Lists._
import Lists.List._
import u04lab.code.Lists.List.Cons // import custom List type (not the one in Scala stdlib)


trait Student {
  def name: String

  def year: Int

  def enrolling(course: Course*): Unit // the student participates to a Course
  def courses: List[String] // names of course the student participates to
  def hasTeacher(teacher: String): Boolean // is the student participating to a course of this teacher?
}

trait Course {
  def name: String

  def teacher: String
}

object Course {

  private case class CourseImpl(val name: String,
                                val teacher: String) extends Course {
  }

  def apply(re: String, im: String): Course = new CourseImpl(re, im)
}

/*Hints:
Simply implement Course, e.g. with a case class
Implement Student with a StudentImpl keeping a private List of
courses
I Use the list implementation used in Ex. 2
Try to implement, in StudentImpl, method courses (with map)
Try to implement, in StudentImpl, method hasTeacher (with map
+ contains)
I Add the method contains to lists for the purpose
Check your solution by running the given main program, comparing
the actual output to the expected one
Refactor the code so that method enrolling accepts a variable
number of courses (variadic argument Course*)*/
object Student {

  private class StudentImpl(val name: String,
                            val year: Int) extends Student {
    private var lst: List[Course] = Nil()

    override def enrolling(course: Course*): Unit = course.foreach(c => lst = Cons(c, lst))

    override def courses: List[String] = map(lst)(_.name)

    override def hasTeacher(teacher: String): Boolean = contains(map(lst)(_.teacher))(teacher)
  }

  def apply(name: String, year: Int = 2017): Student = new StudentImpl(name, year)
}


object Try extends App {
  val cPPS = Course("PPS", "Viroli")
  val cPCD = Course("PCD", "Ricci")
  val cSDR = Course("SDR", "D'Angelo")
  val s1 = Student("mario", 2015)
  val s2 = Student("gino", 2016)
  val s3 = Student("rino") //defaults to 2017
  s1.enrolling(cPPS)
  s1.enrolling(cPCD)
  s2.enrolling(cPPS)
  s3.enrolling(cPPS)
  s3.enrolling(cPCD)
  s3.enrolling(cSDR)
  println(s1.courses, s2.courses, s3.courses) // (Cons(PCD,Cons(PPS,Nil())),Cons(PPS,Nil()),Cons(SDR,Cons(PCD,Cons(PPS,Nil()))))
  println(s1.hasTeacher("Ricci")) // true
}

/** Hints:
 * - simply implement Course, e.g. with a case class
 * - implement Student with a StudentImpl keeping a private Set of courses
 * - try to implement in StudentImpl method courses with map
 * - try to implement in StudentImpl method hasTeacher with map and find
 * - check that the two println above work correctly
 * - refactor the code so that method enrolling accepts a variable argument Course*
 */
