package u04lab.code

import Optionals._
import Lists._

import scala.util.Random

trait PowerIterator[A] {
  def next(): Option[A]
  def allSoFar(): List[A]
  def reversed(): PowerIterator[A]
}

object PowerIterator {

  trait PowerIteratorAbstract[A] extends PowerIterator[A] {

    var lst: List[A] = List.Nil()

    override def allSoFar(): List[A] = List.reverse(lst)

    override def reversed(): PowerIterator[A] = new PowerIteratorsFactoryImpl().fromList(lst)
  }

  private case class PowerIteratorListImpl[A](var list: List[A] = List.Nil()) extends PowerIteratorAbstract[A] {

    override def next(): Option[A] = {
      val actual = List.head(list)
      if (!Option.isEmpty(actual)) {
        list = List.drop(list, 1)
        lst = List.Cons(Option.getOrElse(actual, ???), lst)
      }
      List.head(lst)
    }
  }

  private case class PowerIteratorImpl[A](var actual: A, successive: A => A) extends PowerIteratorAbstract[A] {

    override def next(): Option[A] = {
      lst = List.Cons(actual, lst);
      actual = successive(actual);
      List.head(lst)
    }
  }

  def apply[A](actual: A, successive: A => A): PowerIterator[A] = new PowerIteratorImpl[A](actual, successive)

  def apply[A](list: List[A]): PowerIterator[A] = new PowerIteratorListImpl[A](list)
}

trait PowerIteratorsFactory {

  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]

  def fromList[A](list: List[A]): PowerIterator[A]

  def randomBooleans(size: Int): PowerIterator[Boolean]
}

class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] = PowerIterator(start, successive)

  override def fromList[A](list: List[A]): PowerIterator[A] = PowerIterator[A](list)

  override def randomBooleans(size: Int): PowerIterator[Boolean] = PowerIterator(Random.nextBoolean(), _ => Random.nextBoolean())
}
