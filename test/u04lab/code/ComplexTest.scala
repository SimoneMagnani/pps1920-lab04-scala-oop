package u04lab.code

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class ComplexTest {


  val a = Array(Complex(10,20), Complex(1,1), Complex(7,0))
  val c = a(0) + a(1) + a(2)
  val c2 = a(0) * a(1)

  @Test
  def testComplex() {
    assertEquals(Complex(18.0,21.0),c) // ComplexImpl(18.0,21.0)
    assertEquals(Complex(-10.0,30.0), c2) // ComplexImpl(-10.0,30.0)
    assertEquals("ComplexImpl(-10.0,30.0)",c2.toString)
  }
}