package u04lab.code

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u04lab.code.Lists.List._

class ListTest {


  @Test
  def testFromMultipleArgs() {
    val list = Lists.List(1, 2, 3)
    assertEquals(Cons(1, Cons(2, Cons(3, Nil()))), list)
  }

  @Test
  def testSameTeacher() {
    val cPPS = Course("PPS", "Viroli")
    val cOOP = Course("OOP", "Viroli")
    val cPCD = Course("PCD", "Ricci")
    val cSDR = Course("SDR", "D'Angelo")
    var list = Lists.List(cPPS, cPCD, cSDR, cOOP)
    assertEquals(Cons(cPPS, Cons(cPCD, Cons(cSDR, Cons(cOOP, Nil())))), list)
    assertEquals(null, sameTeacher(list))
    list = Lists.List(cPPS, cPPS, cOOP, cPPS)
    assertEquals("Viroli", Optionals.Option.getOrElse(sameTeacher(list), ""))
  }
}